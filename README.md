sbtw
====
This is intended to be a tool (set of tools) to automatically bootstrap sbt similarly to the
way Gradle does with gradlew and Play does with "play <command>".  The reasoning behind these
tools is to avoid having to use both Gradle and Play to do any kind of Scala project creation.
